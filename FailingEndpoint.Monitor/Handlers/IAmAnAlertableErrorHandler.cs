﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;
using FailingEndpoint.Monitor.Messages.Events;

namespace FailingEndpoint.Monitor.Handlers
{
    public sealed class IAmAnAlertableErrorHandler : IHandleMessages<IAmAnAlertableError>
    {
        public async Task Handle(IAmAnAlertableError message, IMessageHandlerContext context)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            await Task.Run(() => Console.WriteLine($"I am alerting error with id: {message.Id}, message {message.Message}")).ConfigureAwait(false);
        }
    }
}
