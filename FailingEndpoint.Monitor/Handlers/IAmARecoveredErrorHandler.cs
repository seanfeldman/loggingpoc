﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;
using FailingEndpoint.Monitor.Messages.Events;

namespace FailingEndpoint.Monitor.Handlers
{
    public sealed class IAmARecoveredErrorHandler : IHandleMessages<IAmARecoveredError>
    {
        public async Task Handle(IAmARecoveredError message, IMessageHandlerContext context)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            await Task.Run(() => Console.WriteLine($"I am recovered error with id: {message.Id}")).ConfigureAwait(false);
        }
    }
}
