﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using NServiceBus;
using NServiceBus.Persistence.Sql;

namespace FailingEndpoint.Monitor
{
    static class Program
    {
        static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
            return;
        }

        static async Task MainAsync()
        {
            Console.Title = "FailingEndpoint.Monitor";

            var endpointConfiguration = new EndpointConfiguration("FailingEndpoint.Monitor");
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.SendFailedMessagesTo("Errors");
            endpointConfiguration.UseSerialization<JsonSerializer>();

            //endpointConfiguration.Notifications.Errors.MessageHasBeenSentToDelayedRetries += Errors_MessageHasBeenSentToDelayedRetries; ;
            //endpointConfiguration.Notifications.Errors.MessageSentToErrorQueue += Errors_MessageSentToErrorQueue; ;
            //endpointConfiguration.Notifications.Errors.MessageHasFailedAnImmediateRetryAttempt += Errors_MessageHasFailedAnImmediateRetryAttempt;

            var persistence = endpointConfiguration.UsePersistence<SqlPersistence>();
            var connection = "server=xxxxxxxx;user=xxxxxxx;database=monitoringproto_prepod_persistence;port=3306;password=xxxxxx;AllowUserVariables=True;AutoEnlist=false";
            persistence.SqlVariant(SqlVariant.MySql);
            persistence.ConnectionBuilder(() => new MySqlConnection(connection));
            var subscriptions = persistence.SubscriptionSettings();
            subscriptions.CacheFor(TimeSpan.FromMinutes(1));

            //var recoverabilityConfiguration = endpointConfiguration.Recoverability();
            //recoverabilityConfiguration.AddUnrecoverableException<MessageValidationException>();

            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            transport.ConnectionString("host=xxxxxxxxxxxxxx;username=xxxxxxxx;password=xxxxxxxxxx;virtualhost=xxxxxxxxxxxxx;UseTls=true;");
            //transport.Routing().RouteToEndpoint(typeof(UpdateAatpCurrentInventory), Configuration.ServiceBusCommandRoute);
            //transport.Routing().RouteToEndpoint(typeof(UpdateAatpFutureInventory), Configuration.ServiceBusCommandRoute);

            //endpointConfiguration.HeartbeatPlugin(serviceControlQueue: Properties.Settings.Default.ServiceControlInstance);

            var endpointInstance = await Endpoint.Start(endpointConfiguration);
            //BusSession = endpointInstance;
            //await endpointInstance.ScheduleEvery(
            //    timeSpan: TimeSpan.FromSeconds(5),
            //    task: pipelineContext => pipelineContext.SendLocal(new DoSomething() {Id = Guid.NewGuid()})).ConfigureAwait(false);

            //await endpointInstance.SendLocal(new RefreshAatpFutureInventory());

            //await Task.Run(() => CheckStatus());

            Console.WriteLine("Press Any Key To Exit");
            Console.ReadLine();

            await endpointInstance.Stop();
        }
    }
}
